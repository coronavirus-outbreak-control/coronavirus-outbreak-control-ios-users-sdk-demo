//
//  AppDelegate.swift
//  coronavirus-outbreak-control-ios-sample
//
//  Created by Rodrigo Bueno Tomiosso on 23/04/20.
//  Copyright © 2020 mourodrigo. All rights reserved.
//

import UIKit
import coronavirus_outbreak_control_ios_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    public let coronaOutbreakControl = CoronavirusOutbreakControl()
    
    static let shared: AppDelegate = {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { fatalError() }
        return delegate
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        _ = coronaOutbreakControl.application(application, didFinishLaunchingWithOptions: launchOptions)

        return true
    }

}

    extension AppDelegate {
        
        func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            coronaOutbreakControl.application(application, performFetchWithCompletionHandler: completionHandler)
        }

        func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
            coronaOutbreakControl.application(application, willFinishLaunchingWithOptions: launchOptions)
        }

        func applicationWillResignActive(_ application: UIApplication) {
            coronaOutbreakControl.applicationWillResignActive(application)
        }

        func applicationDidEnterBackground(_ application: UIApplication) {
            coronaOutbreakControl.applicationDidEnterBackground(application)
        }

        func applicationWillEnterForeground(_ application: UIApplication) {
            coronaOutbreakControl.applicationWillEnterForeground(application)
        }

        func applicationDidBecomeActive(_ application: UIApplication) {
            coronaOutbreakControl.applicationDidBecomeActive(application)
        }

        func applicationWillTerminate(_ application: UIApplication) {
            coronaOutbreakControl.applicationWillTerminate(application)
        }

        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            coronaOutbreakControl.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
        }
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
            coronaOutbreakControl.application(application, didReceiveRemoteNotification: userInfo)
        }

        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            coronaOutbreakControl.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
        }

    }

