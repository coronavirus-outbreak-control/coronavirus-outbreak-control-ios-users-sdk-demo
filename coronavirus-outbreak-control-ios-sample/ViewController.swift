//
//  ViewController.swift
//  coronavirus-outbreak-control-ios-sample
//
//  Created by Rodrigo Bueno Tomiosso on 23/04/20.
//  Copyright © 2020 mourodrigo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //************************************************************
    // MARK: OUTLETS
    //************************************************************

    @IBOutlet weak var isActiveLabel: UILabel!
    @IBOutlet weak var isBluetoothActivelabel: UILabel!
    @IBOutlet weak var isLocationActivelabel: UILabel!
    @IBOutlet weak var isNotificationActivelabel: UILabel!
    @IBOutlet weak var deviceId: UILabel!
    @IBOutlet weak var isRunning: UILabel!
    @IBOutlet weak var sdkButton: UIButton!
    @IBOutlet weak var isAutoStartLabel: UILabel!
    @IBOutlet weak var ios13LocationContainer: UIStackView!
    @IBOutlet weak var handshakeactivity: UIActivityIndicatorView!
    
    //************************************************************
    // MARK: LIFE CYCLE
    //************************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateStatus()
    }
    
    //************************************************************
    // MARK: UI
    //************************************************************

    private func updateStatus() {
        
        let sdk = AppDelegate.shared.coronaOutbreakControl
        if let did = sdk.getIdentifierDevice(){
            deviceId.text = "Device id: \(did)"
        }
        
        if sdk.getBluetoothPermissionStatus() == .allowed {
            isBluetoothActivelabel.text = "\(String(describing: sdk.getBluetoothPermissionStatus())) \n\(String(describing: sdk.getBluetoothStatus()))"
        } else {
            isBluetoothActivelabel.text = "\(String(describing: sdk.getBluetoothPermissionStatus()))"
        }
        
        isLocationActivelabel.text = String(describing: sdk.getLocationPermissionStatus())
        
        isNotificationActivelabel.text = String(describing: sdk.getNotificationPermissionStatus())
         
        isActiveLabel.text = "SDK can run: " + statusString(for: sdk.canRun)
        isRunning.text = "SDK is running: " + statusString(for: sdk.isRunning)
        
        isAutoStartLabel.text = statusString(for: sdk.isAutoStartEnabled)
        
        sdkButton.setTitle(sdk.isRunning ? "Stop SDK" : "Run SDK", for: .normal)
        
        updateLocationContainerVisibility()
        
        //just a scratch example to how to update the screen... we can use RX here
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.updateStatus()
        }
        
        
    }
    
    private func updateLocationContainerVisibility() {
        let sdk = AppDelegate.shared.coronaOutbreakControl

        var containerIsHidden = true
        
        if  sdk.getLocationPermissionStatus() != .allowedAlways &&
            sdk.getLocationPermissionStatus() != .notDetermined {
            containerIsHidden = false
        }
        
        self.ios13LocationContainer.isHidden = containerIsHidden
    }
    
    private func statusString(for bool: Bool) -> String {
        return bool ? "✅" : "❌"
    }
    
    
    //************************************************************
    // MARK: DEMO ACTIONS
    //************************************************************

    @IBAction func getBluetoothPermission(_ sender: Any) {
        AppDelegate.shared.coronaOutbreakControl
            .requestPermission(requests: [.bluetooth])
    }
    @IBAction func getLocationPermission(_ sender: Any) {
        AppDelegate.shared.coronaOutbreakControl
            .requestPermission(requests: [.location])

    }
    @IBAction func getNotificationPermission(_ sender: Any) {
        AppDelegate.shared.coronaOutbreakControl
            .requestPermission(requests: [.notification])

    }
    @IBAction func toogleAutoStart(_ sender: Any) {
        let isAutoStartEnabled = AppDelegate.shared.coronaOutbreakControl.isAutoStartEnabled
        AppDelegate.shared.coronaOutbreakControl.setAutoStart(enabled: !isAutoStartEnabled)
    }
    
    @IBAction func openSettings(_ sender: Any) {
        AppDelegate.shared.coronaOutbreakControl.openSettings()
    }
    
    @IBAction func handshake(_ sender: Any) {
        handshakeactivity.isHidden = false
        AppDelegate.shared.coronaOutbreakControl.handshake(on: self) { (result) in
            self.handshakeactivity.isHidden = true
            switch result {
                case .success:
                    print("handshake success")
                case .recaptchaError:
                    print("handshake recaptcha error")
                case .serverError:
                    print("handshake server error")
            }
        }
    }
    
    @IBAction func sdkButtonTapped(_ sender: Any) {
        
        if AppDelegate.shared.coronaOutbreakControl.isRunning {
            AppDelegate.shared.coronaOutbreakControl.stop()
        } else {
            AppDelegate.shared.coronaOutbreakControl.start()
        }
        
    }
}

